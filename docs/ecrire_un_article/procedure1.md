---
author: F. Mianes
title: Écrire un article
---
- **Survoler** Contenu / Ajouter du contenu > Cliquer sur Article (site DSDEN).
- Ajouter un titre.
- Ajouter un résumé.
- Image de couverture (facultatif).
- Catégories (facultatif) mais recommandé.
- Mots-clés de remontées (facultatif) mais très recommandé.
- Contenu : ajouter une section puis un module (à voir selon les cas).
- Permission d’accès : à choisir. Si privé, identifiants académiques requis pour la consultation,
- Affichage de l’article (selon les cas) :
    - Ajouter à l'actualité "en ce moment”,
    - Afficher sur le slider en page d'accueil,
    - Apparaît dans le calendrier.
- Cocher ☑ Published (ou décocher si publication ultérieure).
- Enregistrer (ou Preview avant enregistrement).

> Les articles peuvent être écrits à l'avance pour être ensuite associés à un composant (rubrique).