---
author: F. Mianes
title: Accueil
---

# Écrire dans le portail pédagogique du Lot

Vous trouverez ici les procédures à connaitre pour écrire dans le Portail Pédagogique des écoles du Lot.