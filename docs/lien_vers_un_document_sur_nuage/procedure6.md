---
author: F. Mianes
title: Lien vers un document sur Nuage
---
#Lien vers un document sur Nuage

1 - Dans Nuage :

- cocher la case en face du document,
- cliquer sur l'icône de partage,
- en face du lien de partage cliquer sur ➕.

>Le partage **est créé** et **le lien est copié**.

Le principe est le même pour partager un dossier. Son contenu sera partagé. Le configurer en lecture seule.

2 - Dans le Portail :

- ajouter un composant Texte à votre article,
- saisir le texte,
- sélectionner la partie du texte qui sera cliquable,
- cliquer sur l'icône lien 🔗,
- dans URL coller (ctrl v) votre lien de partage créé dans Nuage,
- Enregistrer.

> Pour insérer un lien vers un document se trouvant sur internet il suffit de copier ce lien et de le coller dans l'article.  
On fera un lien direct vers un document plutôt que de l'ajouter en pièce jointe à un article.
