---
author: F. Mianes
title: Associer un article a un composant
---

#Associer un article à un composant

Pré-requis : L’article a déjà été écrit. Il peut comporter un média qui servira d’image d’illustration.

Procédure :

- Accéder au composant.
- Cliquer sur [Edit].
- Dans « Contenu », se placer sur un emplacement de la section et cliquer sur ➕.
- Choisir « Insérer un article ».
- Dans « ARTICLE À RÉFÉRENCER », saisir les premières lettres du titre de l’article à insérer afin de le retrouver.
- Dans Mode d’affichage, sélectionner 🔘 Couverture et décocher ⬜ Baliser le bloc ?.
- Cliquer sur [Enregistrer].