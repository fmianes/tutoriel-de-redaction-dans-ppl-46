---
author: F. Mianes
title: Évènement dans le calendrier
---
#Évènement dans le calendrier

- Écrire un article.
- Cocher : Apparaît dans le calendrier.
- Saisir la date de l'évènement.
- Dans le champs "Choisissez la circonscription concernée" saisir la circonscription ou calendrier départemental.
- Enregistrer.

Un évènement d'une circo peut, si besoin, remonter dans le calendrier départemental.