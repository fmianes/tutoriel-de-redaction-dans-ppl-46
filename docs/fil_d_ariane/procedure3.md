---
author: F. Mianes
title: Fil d'Ariane
---
Cette procédure permet de créer un fil d’Ariane **complet** qui rendra la navigation plus rapide pour les visiteurs.
Un clic permettra d'accéder directement à l'une de ces pages.

Exemple :
<ins>ACCUEIL</ins>/<ins>PÉDAGOGIE</ins>/<ins>DOSSIERS DÉPARTEMENTAUX</ins>/<ins>NUMÉRIQUE ÉDUCATIF</ins>/<ins>RESSOURCES ÉDUCATIVES</ins>/ÉDUCAJOU


**Cette procédure se fait à la fin de la rédaction d’un article.**

- Écrire son article.
- Se rendre dans la section « **État** ».
- Cliquer sur [Alias d’URL].
- Décocher : ⬜ Générer automatiquement un alias d’URL.
- Saisir alors : /pedagogie/dossiers-departementaux/le-nom-de-votre-dossier/le-nom-de-votre-autre-dossier-si-necessaire/le-titre-de-votre-article
- Par exemple, pour l’article qui se nomme « educajou », on saisira :/pedagogie/dossiers-departementaux/numerique-educatif/ressources-educatives/educajou
- Cliquer sur [ENREGISTRER].


!!! Warning "Rédaction du fil d'Ariane"
    Ne pas mettre d’accent.
    Les mots doivent être séparés par un tiret.