---
author: F. Mianes
title: Lien vers une ressource extérieure
---

#Lien vers une ressource extérieure

Copier l'URL de la ressource.

Dans le texte de l'article :

- sélectionner la partie du texte qui sera cliquable,
- cliquer sur l'icône lien 🔗,
- dans "URL du lien" coller l'URL de la ressource et valider avec la coche verte.
- Enregistrer
- Enregistrer