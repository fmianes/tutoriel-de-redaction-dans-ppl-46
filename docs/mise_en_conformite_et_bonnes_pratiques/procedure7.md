---
author: F. Mianes
title: Mise en conformité et bonnes pratiques
---

# Mise en conformité et bonnes pratiques

**Règles RGAA (Référentiel Général d'Amélioration de l'Accessibilité), indispensables à la conformité**

- Contrastes et couleurs (voir outils de vérification en ligne)
- Mises en formes (espacement des lignes, des paragraphes, longueurs des lignes)
- Textes alignés à gauche, pas de justification
- Les listes et la hiérarchie de titres
- Balises alt : Images décoratives (description minimale) / images porteuses de sens (doivent être décrites précisément)
- Intitulés des liens et boutons explicites, relatifs à leur destination et leur action

---

**Les liens URL**

**Les intitulés de liens : explicite en contexte**

Un lien doit avoir un **contexte** pour être compréhensible : un titre, un paragraphe, une phrase, etc.

> Exemple : Si vous ne connaissez pas encore les résultats la coupe du monde féminine, nous vous recommandons de <ins>lire cet article</ins>.

**Les intitulés de liens : explicite hors contexte**

Un lien doit également pouvoir être **compréhensible hors contexte**. Éviter les textes peu clairs comme "'Lire la suite", "En savoir plus", "Cliquer ici",... Il faut préciser la destination du lien.

> Exemple : <ins>En savoir plus sur la coupe du monde féminine.</ins>

Éviter les textes génériques commençant par "Allez à...", "Visitez la page...".

---

**Mention de la présence de l’intelligence artificielle générative dans les contenus**

Si de l'intelligence artificielle générative a été utilisée pour écrire un article ou pour générer une image, il faut l'indiquer.

Pour un texte : ![sparkling](images/sparkling_2_line.svg){ width=1% } Contenu partiellement généré par une IA et vérifié par un agent.

Pour une image : ![sparkling](images/sparkling_2_line.svg){ width=1% } Media partiellement généré par une IA et vérifié par un agent.